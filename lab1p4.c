#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct Node {
    char *data;
    struct Node *next;
};

struct Linkedlist {
    struct Node *header;
};

/*   insertAtBeginning function()
*    Purpose: Creates a node, gives it data, and puts it at the head of the list
*    Note: This method gets called by insertAtEnd if the list is empty
*    Input: The linked list being modified, the string data
*    Output: Modifies the linked list but does not return a value
*/
void insertAtBeginning(struct Linkedlist* LL, char ele[]) {    
    // this method will be called anytime a header node hasn't been created yet (header == NULL)
    if (LL->header == NULL) {
        struct Node *head = (struct Node*)malloc(sizeof(struct Node));
        char *val = (char*)malloc(strlen(ele) * sizeof(char));
        LL->header = head;
        LL->header->data = val;
        LL->header->next = NULL;
        strcpy(LL->header->data, ele);
    } else {
        struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
        char *value = (char*)malloc(strlen(ele) * sizeof(char));
        newNode->next = LL->header;
        LL->header = newNode;
        newNode->data = value;
        strcpy(newNode->data, ele);
    }
}

/*   insertAtEnd function()
*    Purpose: Creates a node, gives it data, and puts it at the end of the list
*    Note: This method gets calls insertAtBeginning if the list is empty
*    Input: The linked list being modified, the string data
*    Output: Modifies the linked list but does not return a value
*/
void insertAtEnd(struct Linkedlist* LL, char ele[]) {

    // special case: No header node exists
    if (LL->header == NULL) {
        insertAtBeginning(LL, ele);
    } else {
        struct Node *ptr = LL->header;

        while(ptr->next != NULL) {
            ptr = ptr->next;
        }
        struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
        char *value = (char*)malloc(sizeof(char));
        ptr->next = newNode;
        newNode->data = value;
        newNode->next = NULL;
        strcpy(newNode->data, ele);
    }
}

/*   deleteNode function()
*    Purpose: Finds node with the given value and removes it.
*    Input: The list being modified, the data value of the node being removed
*    Output: Modifies the linked list but does not return a value
*/
void deleteNode(struct Linkedlist* LL, char ele[]) {
    struct Node *ptr = LL->header;

    // special case: delete header when only node is header
    if (strcmp(ptr->data, ele) == 0 && ptr->next == NULL) {
        free(ptr->data);
        free(ptr);
        LL->header = NULL;
    }

    // special case: delete header when header is not the only node (need to make header the next node)
    else if (strcmp(ptr->data, ele) == 0 && ptr->next != NULL) {
        LL->header = ptr->next;
        free(ptr->data);
        free(ptr);
    }

    if (LL->header != NULL)
        while(ptr->next != NULL) {
            if (strcmp(ptr->next->data, ele) == 0) {
                struct Node *tmp;
                tmp = ptr->next->next;
                free(ptr->next->data);
                free(ptr->next);
                ptr->next = tmp;
                break;
            }
            ptr = ptr->next;
        }
}

/*  findNode() function
    Purpose: Looks for a given string within a given linked list
    Input: Linkedlist being modified, string data being searched for
    Output: 1 if found, 0 if not found
*/
int findNode(struct Linkedlist LL, char ele[]) {
    struct Node *ptr = LL.header;

    if (ptr == NULL) {   // The list is empty-- no value will be found
        return 0;
    }

    while (ptr->next != NULL) {
        if (strcmp(ptr->data, ele) == 0) {
            return 1; // element has been found
        }
        ptr = ptr->next;
    }

    //  statement below checks the last element that the while loop stops at
    if (strcmp(ptr->data, ele) == 0) { 
        return 1;
    }

    return 0; // element not found
}

/* displayLinkedList() function
    Purpose: Prints out data values in order starting at the header
    Input: Linkedlist structure you want to print
    Output: returns nothing, prints the LL data values to the terminal window
*/
void displayLinkedList(struct Linkedlist LL) {
    struct Node *ptr = LL.header;

    if (LL.header == NULL) {
        // printf("The list is empty!");
    }
    else {
        while (ptr->next != NULL) {
            printf("%s, ", ptr->data);
            ptr = ptr->next;
        }
        printf("%s\n", ptr->data);
    }
}

// Loop through the list and free all the memory
void freeListMemory(struct Linkedlist LL) {
    struct Node *ptr = LL.header;

    if (LL.header == NULL) {
        free(ptr);
    } else {
        while(ptr->next != NULL) {
            struct Node *temp = ptr;
            ptr = ptr->next;
            free(temp->data); 
            free(temp);       
        }
    }
    if (ptr != NULL) { // remove from if?
        free(ptr->data);
        free(ptr);
    }
}

/* main() function
    Purpose: Initializes the list, header, and reads in arguments
             Also drives the program by calling the necessary functions
    Input: 1 or more string arguments
    Output: returns exit status and Prints the linked list if successful
*/
int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("ERROR: The program must read at least an argument.\n");
        return EXIT_SUCCESS;
    }
    // Create a Linkedlist structure to modify
    struct Linkedlist *LL = (struct Linkedlist*)malloc(sizeof(struct Linkedlist));
    LL->header = NULL; 

    // Read command line arguments and insert/delete as required
    int i = 1;
    while(i < argc) {
        if (LL->header != NULL && findNode(*LL, argv[i]) != 0) {
            deleteNode(LL, argv[i]);
        }
        else if (isupper(argv[i][0]) != 0) {
            insertAtBeginning(LL, argv[i]);
        }
        else if (islower(argv[i][0]) != 0) {
            insertAtEnd(LL, argv[i]);
        }
        i++;
    }
    printf("The list:- ");
    displayLinkedList(*LL);
    if (LL->header == NULL) {
        printf("\n");        // special case: The list is empty
    }
    freeListMemory(*LL);
    free(LL);

    return EXIT_SUCCESS;
}
